import baseAxios from 'axios';

// configuration
import authenticate from '@/constants/authenticate';
import { auth } from 'auth';

const axios = baseAxios.create({
  baseURL: process.env.BASE_API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});
const session = auth();

axios.interceptors.request.use(
  function (config) {
    if (!config.headers['Authorization']) {
      //   config.headers['Authorization'] = `Bearer ${session?.user?.token}`;
    }

    // if (userData) {
    //   userData = JSON.parse(userData)
    //   config.headers['X-Klp-Id'] = klpId || userData.default_klp.id
    // }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  },
);
