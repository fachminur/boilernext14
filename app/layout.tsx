import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import { Inter as FontSans } from 'next/font/google';
import { Toaster } from '@/components/ui/toaster';
import Providers from '@/components/layout/providers';
import { auth } from 'auth';

// const inter = Inter({ subsets: ["latin"] });
const fontSans = FontSans({
  subsets: ['latin'],
  variable: '--font-sans',
});

export const metadata: Metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app',
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  // const session = await getServerSession();
  const session = await auth();
  return (
    <html lang="en">
      <body className={fontSans.variable}>
        <Providers session={session}>
          <Toaster />
          {children}
        </Providers>
      </body>
    </html>
  );
}
