// import type { NextAuthConfig } from 'next-auth';
// import NextAuth from 'next-auth';
// import CredentialsProvider from 'next-auth/providers/credentials';

// const credentialsConfig = CredentialsProvider({
//   name: 'Credentials',
//   credentials: {},
//   async authorize(credentials) {
//     console.log('authorizationn', credentials);
//     // if (credentials.username === 'sk' && credentials.password === '123')
//     // return {
//     //   name: 'Vahid',
//     //   token: credentials.token,
//     // };
//     // else return null;
//     return { ...credentials };
//   },
// });

// const config = {
//   providers: [credentialsConfig],
//   //   callbacks: {
//   //     authorized({ request, auth }) {
//   //       const { pathname } = request.nextUrl;
//   //       if (pathname === '/middlewareProtected') return !!auth;
//   //       return true;
//   //     },
//   //   },
//   //   callbacks: {
//   //     async signIn({ user, account, profile, email, credentials }) {
//   //       return true;
//   //     },
//   //     async jwt({ token, user, trigger, session }) {
//   //       if (trigger === 'update') {
//   //         // console.log('session_trigger', session);
//   //         return {
//   //           ...token,
//   //           ...session.user,
//   //           userInfo: JSON.stringify(session.user.userInfo),
//   //           role: JSON.stringify(session.user.role),
//   //           //permissions: JSON.stringify(session.user.permissions)
//   //         };
//   //       }
//   //       return {
//   //         ...token,
//   //         ...user,
//   //       };
//   //     },
//   //     async session({ session, user, token: { userInfo: any } }) {
//   //       const data: any = <any>{
//   //         ...token,
//   //         userInfo: JSON.parse(token.userInfo),
//   //         role: JSON.parse(token.role),
//   //         //permissions: JSON.parse(token.permissions)
//   //       };
//   //       session.user = data;
//   //       return session;
//   //     },
//   //     // async redirect({ url, baseUrl }) {
//   //     //   return "https://webmanager-dev.bpkp.go.id"
//   //     //   // return "http://10.10.20.73:3030"
//   //     //   // return baseUrl
//   //     // },
//   //   },
// } satisfies NextAuthConfig;

// export const { handlers, auth, signIn, signOut } = NextAuth(config);

import NextAuth from 'next-auth';
import { authConfig } from './auth.config';
import Credentials from 'next-auth/providers/credentials';
import { User } from '@/lib/user';

export const { handlers, auth, signIn, signOut } = NextAuth({
  ...authConfig,
  providers: [
    Credentials({
      async authorize(credentials) {
        if (credentials.username && credentials.password) {
          // Add you backend code here
          // let loginRes = await backendLogin(credentials.id, credentials.password)
          console.log(credentials);
          let loginRes = {
            success: true,
            data: {
              user: {
                ID: 'john_doe',
                NAME: 'John Doe',
                EMAIL: 'email@email.email',
              },
            },
          };
          // Failed logging in
          if (!loginRes.success) return null;
          // Successful log in
          const user = {
            id: loginRes.data.user.ID ?? '',
            name: loginRes.data.user.NAME ?? '',
            email: loginRes.data.user.EMAIL ?? '',
          } as User;
          return user;
        }
        return null;
      },
    }),
  ],
  callbacks: {
    async session({ session, token, user }) {
      session.user = token.user as User;
      return session;
    },
    async jwt({ token, user, trigger, session }) {
      if (user) {
        token.user = user;
      }
      return token;
    },
  },
});
