'use client';
import { Button } from '@/components/ui/button';
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import validation_message from '@/constants/validation';
import { zodResolver } from '@hookform/resolvers/zod';
import { EyeClosedIcon, EyeOpenIcon, PersonIcon } from '@radix-ui/react-icons';
import { useSearchParams } from 'next/navigation';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import * as z from 'zod';
import axios from 'axios';
import { useToast } from '../ui/use-toast';
import { cn } from '@/lib/utils';
import { signIn } from 'next-auth/react';

const formSchema = z.object({
  username: z.string().trim().min(1, {
    message: validation_message.required_error,
  }),
  password: z.string().min(1, {
    message: validation_message.required_error,
  }),
});

type UserFormValue = z.infer<typeof formSchema>;

export default function UserAuthForm() {
  const searchParams = useSearchParams();
  const callbackUrl = searchParams.get('callbackUrl');
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const defaultValues = {
    username: 'fahmi nur rahman',
    password: 'L@ziel4213',
  };
  const form = useForm<UserFormValue>({
    resolver: zodResolver(formSchema),
    defaultValues,
  });
  const { toast } = useToast();

  const onSubmit = async (data: UserFormValue) => {
    // console.log(data);
    try {
      axios
        .post('http://recol-api-website-ng-cms.localhost/cms/api/auth/login', {
          username: data.username,
          password: data.password,
        })
        .then((response) => {
          // console.log(response.data);
          // console.log(response.status);
          if (response.status) {
            console.log(response.data.data.user);
            console.log(response.data.data.token);
            console.log(response.data.data.roles);
            signIn(
              'credentials',
              // data,
              {
                token: response.data.data.token,
                username: data.username,
                password: data.password,
                // userInfo: JSON.stringify(response.data.data.user),
                // role: JSON.stringify(response.data.data.roles),
                // permissions: JSON.stringify(permission.data.data),
                // redirect: true,
              },
            );
          } else {
            toast({
              title: 'Uh oh! Something went wrong.',
              description: 'There was a problem with your request.',
              className: cn('top-0 right-0 flex fixed md:max-w-[420px] md:top-4 md:right-4'),
            });
          }
        });
    } catch (error) {
      if (error) {
        // console.log(error);
        // toastError(error.response.data.message);
        toast({
          title: 'Uh oh! Something went wrong.',
          description: 'There was a problem with your request.',
          className: cn('top-0 right-0 flex fixed md:max-w-[420px] md:top-4 md:right-4'),
        });
      }
    }
    // signIn('credentials', {
    //   username: data.username,
    //   password: data.password,
    //   // callbackUrl: callbackUrl ?? '/dashboard',
    //   callbackUrl: process.env.BASE_URL,
    // });
  };

  return (
    <>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="w-full space-y-2">
          <FormField
            control={form.control}
            name="username"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Username</FormLabel>
                <FormControl>
                  <div className="flex w-full max-w-sm items-center space-x-2">
                    <Input
                      type="text"
                      placeholder="Enter your email..."
                      disabled={loading}
                      {...field}
                    />
                    <PersonIcon height={24} width={24} />
                  </div>
                </FormControl>
                <FormDescription>This is your public display name.</FormDescription>
                <FormMessage />
              </FormItem>
            )}
          />

          <FormField
            control={form.control}
            name="password"
            render={({ field }) => (
              <FormItem>
                <FormLabel>Password</FormLabel>
                <FormControl>
                  <div className="flex w-full max-w-sm items-center space-x-2">
                    <Input
                      type={showPassword ? 'text' : 'password'}
                      placeholder="Enter your password..."
                      disabled={loading}
                      {...field}
                    />
                    {showPassword ? (
                      <EyeOpenIcon onClick={() => setShowPassword(false)} height={24} width={24} />
                    ) : (
                      <EyeClosedIcon onClick={() => setShowPassword(true)} height={24} width={24} />
                    )}
                  </div>
                </FormControl>
                <FormMessage />
              </FormItem>
            )}
          />

          <Button disabled={loading} className="ml-auto w-full" type="submit">
            Login
          </Button>
        </form>
      </Form>
      <Button
        variant="outline"
        onClick={() => {
          toast({
            title: 'Uh oh! Something went wrong.',
            description: 'There was a problem with your request.',
            className: cn('top-0 right-0 flex fixed md:max-w-[420px] md:top-4 md:right-4'),
          });
        }}
      >
        Show Toast
      </Button>
      {/* <div className="relative">
        <div className="absolute inset-0 flex items-center">
          <span className="w-full border-t" />
        </div>
        <div className="relative flex justify-center text-xs uppercase">
          <span className="bg-background px-2 text-muted-foreground">
            Or continue with
          </span>
        </div>
      </div> */}
      {/* <GoogleSignInButton /> */}
    </>
  );
}
