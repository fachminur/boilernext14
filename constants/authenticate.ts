export default {
  loginEndpoint: '/api/login',
  profileEndpoint: '/api/profile',
  tokenNameKey: 'accessToken',
  onTokenExpired: 'refrestToken',
};
