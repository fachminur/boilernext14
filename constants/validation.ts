export default {
  invalid_type_error: 'Invalid type provided for this field',
  required_error: 'This field is required',
};
